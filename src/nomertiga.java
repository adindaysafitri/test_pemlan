public class nomertiga {

    public static void main(String[] args) {
        //3. pengolahan data array dua dimensi
        int [][] nilai = {{60,75,88,50}, {65,50,82,45}, {70,59,90,60},{90,85,88,80}};
        String [] nama = {"Dimas","Rio","Kevin","Haris"};
        double [] nilaiKuis = new double[4];
        double [] rataKelas = new double[4];
        //a. Menghitung nilai Kuis rata-rata masing-masing mahasiswa
        for(int i=0;i<4;i++){
            for(int j=0;j<4;j++){
                nilaiKuis[i] += nilai[i][j];//menyimpan data dari variabel array "nilai" ke variabel array baru "nilaiKuis" sekaligus menjumlahkan isi data
            }
            nilaiKuis[i] /= 4; //membagi nilai dari tiap data pada variabel array "nilaiKuis"
            System.out.println("Rata-rata nilai "+ nama[i] + " = "+ nilaiKuis[i]);
        }
        System.out.println();
        //b. Menghitung nilai kuis rata-rata kelas
        for(int i=0;i<4;i++){
            for(int j=0;j<4;j++){
                rataKelas[i] += nilai[j][i];
            }
            rataKelas[i] /= 4;
            System.out.println("Rata-rata nilai kuis kelas ke-"+ (i+1)+ " = "+rataKelas[i]);
        }
        System.out.println();
        //c. Menentukan nama mahasiswa dengan nilai kuis rata-rata tertinggi
        double max;
        String namaTerbesar = "";
        max = nilaiKuis[0];
        for(int i=0;i<4;i++){
            if(nilaiKuis[i] > max){
                max = nilaiKuis[i];
                namaTerbesar = nama[i];
            }
        }System.out.println("Mahasiswa dengan nilai rata-rata terbesar adalah "+ namaTerbesar);
    }
}
