package com.company;
public class Main {

    public static void main(String[] args) {
        //1. Mengurutkan data array
        System.out.print("Data array sebelum diurutkan = ");
        System.out.print("5 7 8 9 6 4 3 2 9");
        int [] a = {5, 7, 8, 9, 6, 4, 3, 2, 9};
        //setelah diurutkan
        for (int i = 0; i < a.length; i++) {
            for (int j = i + 1; j < a.length; j++) {
                if (a[i] > a[j]) {
                    int temp = a[i];
                    a[i] = a[j];
                    a[j] = temp;
                }
            }
        }
        System.out.print("\nData array setelah diurutkan = ");
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
    }
}

